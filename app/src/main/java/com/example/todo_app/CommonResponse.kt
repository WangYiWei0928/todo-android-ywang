package com.example.todo_app

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CommonResponse(
    var errorCode: Int,
    var errorMessage: String
) : Parcelable