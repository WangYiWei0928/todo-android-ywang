package com.example.todo_app

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.todo_app.databinding.FragmentTodoEditBinding
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class TodoEditFragment : Fragment() {
    private lateinit var binding: FragmentTodoEditBinding
    private val args: TodoEditFragmentArgs by navArgs()
    private val formatter = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTodoEditBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val todo = args.todo
        todo?.let {
            setUpTexts(it)
        }
        setUpRegistrationButton(todo?.id)

        binding.registrationButton.isEnabled = false
        setUpTextChangedListener()
        binding.textDate.setOnClickListener {
            showDatePicker()
        }
    }

    private fun setUpTextChangedListener() {
        binding.editTextTitle.addTextChangedListener {
            updateRegistrationButtonState()
            setUpTitleTextCount(it?.length ?: 0)
        }

        binding.editTextDetail.addTextChangedListener {
            updateRegistrationButtonState()
            setUpDetailTextCount(it?.length ?: 0)
        }
    }

    private fun updateRegistrationButtonState() {
        val isValidTitleTextLength = binding.editTextTitle.length() in 1..TITLE_MAX_LENGTH
        val isValidDetailTextLength = binding.editTextDetail.length() in 0..DETAIL_MAX_LENGTH
        binding.registrationButton.isEnabled = isValidTitleTextLength && isValidDetailTextLength
    }

    private fun setUpRegistrationButton(id: Int?) {
        val stringResource = if (id == null) R.string.registration else R.string.update
        binding.registrationButton.text = getString(stringResource)
        binding.registrationButton.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                sendTodo(id)
            }
        }
    }

    private fun setUpTexts(todo: Todo) {
        binding.editTextTitle.setText(todo.title)
        todo.detail?.let {
            binding.editTextDetail.setText(it)
        }
        todo.date?.let {
            binding.textDate.text = formatter.format(it)
        }
    }

    private fun setUpTitleTextCount(titleTextLength: Int) {
        binding.textTitleCount.text = titleTextLength.toString()
        val titleCountColor = if (titleTextLength <= TITLE_MAX_LENGTH) Color.GRAY else Color.RED
        binding.textTitleCount.setTextColor(titleCountColor)
    }

    private fun setUpDetailTextCount(detailTextLength: Int) {
        binding.textDetailCount.text = detailTextLength.toString()
        val detailCountColor = if (detailTextLength <= DETAIL_MAX_LENGTH) Color.GRAY else Color.RED
        binding.textDetailCount.setTextColor(detailCountColor)
    }

    private fun showDatePicker() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            requireContext(),
            { _, y, m, d ->
                binding.textDate.text = ("$y/${m + 1}/$d")
            }, year, month, day
        )

        datePickerDialog.setButton(
            DialogInterface.BUTTON_NEUTRAL,
            getString(R.string.btn_date_clear)
        ) { _, _ ->
            binding.textDate.text = ""
        }
        datePickerDialog.show()
    }

    private fun makeTodoRequestBody(): TodoRequestBody {
        val title = binding.editTextTitle.text.toString()
        val detail =
            if (binding.editTextDetail.text.isBlank()) null else binding.editTextDetail.text.toString()

        val date =
            if (binding.textDate.text.isBlank()) {
                null
            } else {
                try {
                    formatter.parse(binding.textDate.text.toString())
                } catch (e: Exception) {
                    Log.e("", getString(R.string.get_date_path_failed), e)
                    null
                }
            }
        return TodoRequestBody(title, detail, date)
    }

    private suspend fun sendTodo(id: Int?) {
        try {
            val request = ApiClient().apiRequest
            val response = if (id == null) {
                request.addTodo(makeTodoRequestBody()).execute()
            } else {
                request.updateTodo(id, makeTodoRequestBody()).execute()
            }

            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    onResponseSuccess(id != null)
                }
            } else {
                onResponseFailed()
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorAlertDialog(getString(R.string.error_message))
            }
        }
    }

    private fun onResponseSuccess(isUpdate: Boolean) {
        val message = if (isUpdate) getString(R.string.update) else getString(R.string.registration)

        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.value = Event(getString(R.string.snackbar_message, message))
        findNavController().popBackStack()
    }

    private suspend fun onResponseFailed() {
        val gson =
            GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()
        val body = gson.fromJson(
            ApiClient().apiRequest.addTodo(makeTodoRequestBody()).execute().errorBody()
                ?.string(), CommonResponse::class.java
        )
        withContext(Dispatchers.Main) {
            showErrorAlertDialog(body.errorMessage)
        }
    }

    companion object {
        private const val TITLE_MAX_LENGTH = 100
        private const val DETAIL_MAX_LENGTH = 1000
        private const val DATE_FORMAT = "yyyy/MM/dd"
    }
}