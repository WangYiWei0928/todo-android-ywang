package com.example.todo_app

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class TodoRequestBody(
    val title: String,
    val detail: String?,
    val date: Date?
) : Parcelable