package com.example.todo_app

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> LiveData<Event<T>>.observeEvent(owner: LifecycleOwner, observer: (T) -> Unit) {
    observe(owner, Observer {
        val content = it.getContentIfNotHandLed() ?: return@Observer
        observer.invoke(content)
    })
}