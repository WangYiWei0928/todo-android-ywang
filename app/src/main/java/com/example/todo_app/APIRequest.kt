package com.example.todo_app

import retrofit2.Call
import retrofit2.http.*

interface APIRequest {
    @GET("todos")
    fun fetchTodos(): Call<TodosGetResponse>

    @POST("todos")
    fun addTodo(@Body todoRequestBody: TodoRequestBody): Call<CommonResponse>

    @PUT("todos/{id}")
    fun updateTodo(
        @Path("id") id: Int,
        @Body todoRequestBody: TodoRequestBody
    ): Call<CommonResponse>

    @DELETE("todos/{id}")
    fun deleteTodo(@Path("id") id: Int): Call<CommonResponse>
}