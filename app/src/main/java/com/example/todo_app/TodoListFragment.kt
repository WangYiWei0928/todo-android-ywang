package com.example.todo_app

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todo_app.databinding.FragmentTodoListBinding
import com.google.android.material.snackbar.Snackbar
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import kotlinx.coroutines.*
import java.lang.Exception

class TodoListFragment : Fragment() {
    private lateinit var binding: FragmentTodoListBinding
    private val adapter = RecyclerAdapter(::onClickTodoItem)
    private var isDeleteMode = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.observeEvent(this) {
            Snackbar.make(requireView(), it, Snackbar.LENGTH_SHORT).show()
        }
        binding = FragmentTodoListBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.also {
            val layout = LinearLayoutManager(activity)
            it.layoutManager = layout
            it.adapter = adapter
            it.addItemDecoration(DividerItemDecoration(activity, layout.orientation))
            CoroutineScope(Dispatchers.Default).launch {
                fetchTodoList()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_todo_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_create -> {
                goTodoEditFragment()
                isDeleteMode = false
                true
            }
            R.id.menu_delete -> {
                isDeleteMode = !isDeleteMode
                val icon = if (isDeleteMode) R.drawable.ic_done else R.drawable.ic_delete
                item.setIcon(icon)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showDeleteAlertDialog(id: Int, title: String) {
        AlertDialog.Builder(context)
            .setTitle(getString(R.string.todo_delete_message, title))
            .setNegativeButton(R.string.dialog_btn_cancel, null)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                CoroutineScope(Dispatchers.IO).launch {
                    deleteTodo(id)
                }
            }
            .show()
    }

    private fun onClickTodoItem(todo: Todo) {
        if (isDeleteMode) {
            showDeleteAlertDialog(todo.id, todo.title)
        } else {
            goTodoEditFragment(todo)
        }
    }

    private fun goTodoEditFragment(todo: Todo? = null) {
        val action = TodoListFragmentDirections.actionFragmentTodoListToTodoEditFragment(todo)
        findNavController().navigate(action)
    }

    private suspend fun fetchTodoList() {
        try {
            val response = ApiClient().apiRequest.fetchTodos().execute()
            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    adapter.todoList = response.body()?.todos!!
                }
            } else {
                onResponseFailed()
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorAlertDialog(getString(R.string.error_message))
            }
        }
    }

    private suspend fun deleteTodo(id: Int) {
        try {
            val response = ApiClient().apiRequest.deleteTodo(id).execute()
            if (response.isSuccessful) {
                fetchTodoList()
            } else {
                onResponseFailed()
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                showErrorAlertDialog(getString(R.string.error_message))
            }
        }
    }

    private suspend fun onResponseFailed() {
        val gson =
            GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()
        val body = gson.fromJson(
            ApiClient().apiRequest.fetchTodos().execute().errorBody()
                ?.string(), CommonResponse::class.java
        )
        withContext(Dispatchers.Main) {
            showErrorAlertDialog(body.errorMessage)
        }
    }
}