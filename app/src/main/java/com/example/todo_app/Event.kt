package com.example.todo_app

class Event<out T>(private val content: T) {
    private var hasBeenHandLed = false

    fun getContentIfNotHandLed(): T? {
        return if (hasBeenHandLed) {
            null
        } else {
            hasBeenHandLed = true
            content
        }
    }
}